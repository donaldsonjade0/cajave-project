import json


class Account:
    """
    Class that contains account's information
    """
    def __init__(self, user_name, password, email, client_id):
        """
        Initializing account object

        :param user_name: account's user name
        :param password: account's password
        :param email: client's email
        :param client_id: id of client who owns the account
        """
        self.user_name = user_name
        self.passw = password
        self.email = email
        self.client_id = client_id

    @staticmethod
    def deserialize(json_str=None, json_dict=None):
        """
        Making Account object from json string or dictionary

        :param json_str: json string
        :param json_dict: dictionary
        :return: Account object
        """
        if json_dict is None:
            json_dict = json.loads(json_str)
        return Account(id=json_dict['id'] if 'id' is json_dict.keys() else None,
                       user_name=json_dict['user_name'],
                       password=json_dict['password'],
                       email=json_dict['email'],
                       client_id=json_dict['client_id'])

    def serialize(self):
        """
        Making json from Account object

        :return: json string
        """
        return json.dumps(self.__dict__)
