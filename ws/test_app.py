import pytest
import requests

base_url = 'http://127.0.0.1:5000'


def test_main():
    response = requests.get(f'{base_url}/')

    assert response.ok
    assert response.content == b'Hello, World!'

def test_login():
    response = requests.get(f'{base_url}/api/login')
    assert response.status_code == 200
