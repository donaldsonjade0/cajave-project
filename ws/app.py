import json
import os

from flask import Flask, Response, request, jsonify
from flask_cors import CORS
import requests
from sseclient import SSEClient
import hashlib

app = Flask(__name__)
CORS(app)

if os.name == 'nt':
    app.config['DATAGEN_URL'] = 'http://datagennew-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com'
    app.config['DAO_URL'] = 'http://daodb-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com/'
    app.config['APP_PORT'] = 5001
else:
    app.config['DATAGEN_URL'] = os.environ['DATAGEN_URL']
    app.config['DAO_URL'] = os.environ['DAO_URL']
    app.config['APP_PORT'] = os.environ['APP_PORT']


# set-up
@app.route('/')
def main():
    return 'We are working.'


# login api endpoint
@app.route('/api/login', methods=['POST'])
def login():
    # retrieves login credentials from front-end
    req = request.json
    code = send_login(req['username'], req['password'])

    # validate logins
    if code == 200:
        return Response(status=200)
    else:
        return Response(status=401)


# registration api endpoint
@app.route('/api/register', methods=['POST'])
def registration():
    # handle new registration info
    req = request.json
    print(req['firstname'])
    print(req['lastname'])
    print(req['username'])
    print(req['password'])

    code = send_registration(req['firstname'], req['lastname'], req['username'], req['password'], req['email'],
                             req['role'])
    Response(status=200)
    # validate registration
    if code == 200:
        return Response(status=200)
    else:
        return Response(status=401)


@app.route('/api/deals', methods=['GET'])
def get_deals_stream():
    def deal_stream():
        messages = SSEClient(app.config['DATAGEN_URL'])
        for msg in messages:
            yield 'data: ' + str(msg) + '\n\n'

    return Response(deal_stream(), mimetype="text/event-stream")


def send_login(user, password):
    # send login to dao
    info = {
        'user_name': user,
        'password': password
    }
    header = {'Content-Type': 'application/json'}
    r = requests.post(f'{app.config["DAO_URL"]}/login', json=info, headers=header)
    print(r.status_code)
    return r.status_code


def send_registration(fname, lname, user, password, email, role):
    # send registration info to dao
    print(fname)
    info = {'user_name': user, 'password': password, 'email': email, 'role': role, 'first_name': fname,
            'last_name': lname}
    header = {'Content-Type': 'application/json'}
    r = requests.post(f'{app.config["DAO_URL"]}/signup', json=info, headers=header)
    print(r.status_code)
    return r.status_code


@app.route('/api/calculations', methods=['POST'])
def data_calculations():
    req = request.json

    return


@app.route('/api/avg_price/<product_name>', methods=['GET'])
def calc_avg_price(product_name):
    url = f'{app.config["DATAGEN_URL"]}/calc_avg_price/{product_name}'
    print(url)
    response = requests.get(url)
    print(response.content)
    return response.content

"""@app.route('/newdeal', methods=['POST'])
def data_to_frontend():
    a_variable = insert()
    return"""

if __name__ == '__main__':
    get_deals_stream()
    app.run(host='0.0.0.0', port=app.config['APP_PORT'], debug=True)
