import pytest
import requests

base_url = 'http://127.0.0.1:5002'


# Test for functions in dao/app.py

def test_insert():
    response = requests.post(f'{base_url}/deal',
                             json='{"deal_id": null, "instrumentName": "Lunatic", "cpty": "Lucy", "price": 1567.8767, ' \
                                  '"type": "S", "quantity": 1, "time": "11-Aug-2019 (13:15:29.497437)"}')

    assert response.ok


def test_insert_client():
    response = requests.post(f'{base_url}/client', json='{"name": "Joshua"}')

    assert response.ok


def test_sign_up():
    info = {"user_name": "lucy23", "password": "qwerty","email":"lucy@mail.ru", "role":'Trader', "first_name":"Lucy", "last_name":"Green"}
    response = requests.post(f'{base_url}/signup', json=info)

    assert response.ok


def test_get_deal_by_id():
    response = requests.get(f'{base_url}/deal/id/1')
    assert response.ok



def test_login():
    response = requests.post(f'{base_url}/login', json={"user_name":"lucy690", "password":"qwerty"})

    assert response.ok

def test_get_by_name():
    response = requests.get(f'{base_url}/deal/name/Lucy')
    print(response.content)

    assert response.ok

def test_get_by_product():
    response = requests.get(f'{base_url}/deal/product/Lunatic')
    print(response.content)

    assert response.ok

def test_get_products_prices():
    response = requests.get(f'{base_url}/product')
    print(response.content)

    assert response.ok

