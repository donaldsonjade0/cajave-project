import json


class Account:
    def __init__(self, user_name, password, email, client_id, id=None):
        self.user_name = user_name
        self.passw = password
        self.email = email
        self.client_id = client_id

    @staticmethod
    def deserialize(json_str=None, json_dict=None):
        if json_dict is None:
            json_dict = json.loads(json_str)
        return Account(id=json_dict['id'] if 'id' is json_dict.keys() else None,
                       user_name=json_dict['user_name'],
                       password=json_dict['password'],
                       email=json_dict['email'],
                       client_id=json_dict['client_id'])

    def serialize(self):
        return json.dumps(self.__dict__)
