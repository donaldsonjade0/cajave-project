class DbTable:
    """
    Class DbTable with methods for sql commands
    """
    def __init__(self, cursor, db, table_name):
        """
        Initialising DbTable

        :param cursor: cursor of database
        :param db: database
        :param table_name: name of table
        """
        self.cursor = cursor
        self.db = db
        self.table_name = table_name


    def query(self, sql_statement, data, type_command):
        """
        Doing all queries

        :param sql_statement: sql command
        :param data: data to be pushed to sql command
        :param type_command: type of command
        :return: if it is a select command than returns all that was selected else None
        """
        if data is not None:
            self.cursor.execute(sql_statement, data)
        else:
            self.cursor.execute(sql_statement)
        if type_command == 'SELECT':
            return self.cursor.fetchall()
        else:
            return None

    def insert(self, data_dict):
        """
        SQL command INSERT

        :param data_dict: data to insert
        :return: None
        """
        key_str = ','.join("{!s}".format(k) for k in data_dict.keys())
        val_str = ''
        for vals in data_dict.values():
            val_str += '%s,'
        val_str = val_str[:len(val_str)-1]
        sql_str = 'INSERT INTO ' + self.table_name + ' (' + key_str + ') VALUES (' + val_str + ')'
        self.query(sql_str, tuple(data_dict.values()), 'INSERT')
        self.db.commit()

    def make_where_str(self, where_statement=None, where_data_dict=None):
        """
        Making string for that should go after word WHERE in different types of SQL commands

        :param where_statement: conditions not using any data
        :param where_data_dict: conditions using some data
        :return: string to put to sql command
        """
        if where_statement is not None and where_data_dict is not None:
            where_str = 'WHERE ' + where_statement
            for column in where_data_dict.keys():
                where_str += ' and '
                where_str += column
                where_str += '=%s '
        elif where_data_dict is not None:
            where_str = ''
            for column in where_data_dict.keys():
                where_str += 'and '
                where_str += column
                where_str += '=%s '
            where_str = 'WHERE '+ where_str[4:]
        elif where_statement is not None:
            where_str = 'WHERE ' + where_statement
        else:
            where_str = ''
        return where_str


    def update(self, column_name, data, where_statement=None, where_data_dict=None, operation=None):
        """
        SQL command UPDATE

        :param column_name: column name to update
        :param data: data for update
        :param where_statement: conditions not using any data
        :param where_data_dict:
        :param operation: conditions using some data
        :return: None
        """
        where_str = self.make_where_str(where_statement, where_data_dict)

        if operation is None:
            sql_str = 'UPDATE ' + self.table_name + ' SET ' + column_name +  '= %s ' + where_str
        elif operation == '+':
            sql_str = 'UPDATE ' + self.table_name + ' SET ' + column_name + '=' + column_name +'+ %s ' + where_str
        elif operation == '-':
            sql_str = 'UPDATE ' + self.table_name + ' SET ' + column_name + '=' + column_name + '- %s ' + where_str

        data_list = [data]
        data_list.extend(list(where_data_dict.values()))
        data_tuple = tuple(data_list)
        self.query(sql_str, data_tuple, 'UPDATE')
        self.db.commit()

    def select(self, names, where_data_dict=None, tables=None, where_statement=None):
        """
        SQL command SELECT

        :param names: names of columns to select
        :param where_data_dict: conditions using some data
        :param tables: tables from which selection will be done
        :param where_statement: conditions not using any data
        :return: result of selection
        """
        if tables is None:
            tables = [self.table_name]

        names_str = ','.join("{!s}".format(k) for k in names)
        tables_str = ','.join("{!s}".format(k) for k in tables)
        where_str = self.make_where_str(where_statement, where_data_dict)

        sql_str = 'SELECT ' + names_str + ' FROM ' + tables_str + ' ' + where_str
        if where_data_dict is not None:
            resp = self.query(sql_str, tuple(where_data_dict.values()), 'SELECT')
        else:
            resp = self.query(sql_str, None, 'SELECT')
        self.db.commit()
        return resp

    def insert_if_not_exists(self, data_dict, unique_columns):
        """
        Inserts if there is no element with the same unique columns

        :param data_dict: data to insert
        :param unique_columns: columns which should be unique
        :return: response if insert command was successful
        """
        names = ['count(*)']
        where_data_dict = {}
        for column in unique_columns:
            where_data_dict[column] = data_dict[column]

        resp = self.select(names, where_data_dict=where_data_dict)

        if resp[0][0] == 0:
            self.insert(data_dict)
            self.db.commit()
            return 200
        else:
            return 409



