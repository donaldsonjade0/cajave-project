from flask import Flask, request, jsonify, json, Response

from deal import Deal
from deal_dao import insert_deal_to_db, select_deal_by_id, select_deals_by_name, select_deals_by_product

from account import Account
from account_dao import insert_account_to_db, check_password

from client_dao import insert_client_to_db, find_client_id

from instrument_dao import select_product_prices

import mysql.connector
from dao_helper import DbTable
import os

app = Flask(__name__)

# config
if os.name == 'nt':
    user_name = 'root'
    password = 'ppp'
    db_host = '192.168.99.100'
    db_name = 'Case_Study_db'
    app.config['APP_PORT'] = 5002
else:
    user_name = os.environ['DB_USERNAME']
    password = os.environ['DB_PASSWORD']
    db_host = os.environ['DB_HOST']
    db_name = os.environ['DB_NAME']
    app.config['APP_PORT'] = os.environ['APP_PORT']

# Conection to database
mydb = mysql.connector.connect(user=user_name, password=password,
                               host=db_host,
                               database=db_name)

cursor = mydb.cursor()

# Creating database tables
deals_table = DbTable(cursor, mydb, 'Deals')
accounts_table = DbTable(cursor, mydb, 'Accounts')
clients_table = DbTable(cursor, mydb, 'Clients')
instruments_table = DbTable(cursor, mydb, 'Instruments')
inventories_table = DbTable(cursor, mydb, 'Inventories')


@app.route('/deal', methods=['POST'])
def insert_deal():
    """
    Inserts new deal by client's request

    :return: response how successful request was
    """
    data = request.json
    deal = Deal.deserialize(data)
    insert_deal_to_db(deal, deals_table, instruments_table, clients_table, inventories_table)
    return Response(status=200)


@app.route('/deal/id/<id>', methods=['GET'])
def get_deal_by_id(id):
    """
    Gets deal by id by client's request

    :param id: deal id that client want
    :return: deal information
    """
    # data = request.json
    # id = json.loads(data)['id']
    deal = select_deal_by_id(id, deals_table, instruments_table, clients_table)
    if deal is None:
        return Response(status=404)
    else:
        resp = deal.serialize()
        return resp


@app.route('/deal/name/<name>', methods=['GET'])
def get_deal_by_name(name):
    """
    Gets all client's deal by client's request

    :param name:client's name
    :return:list of client's deals
    """
    deals_list = select_deals_by_name(name, deals_table)
    return jsonify(list(x.serialize() for x in deals_list))


@app.route('/deal/product/<product>', methods=['GET'])
def get_deals_by_product(product):
    """
    Gets all deals with certain instrument

    :param product: instrument's name
    :return: list of deals with this instrument
    """
    deals_list = select_deals_by_product(product, deals_table)
    return jsonify(list(x.serialize() for x in deals_list))


@app.route('/signup', methods=['POST'])
def sign_up():
    """
    Registration of new client

    :return: response if regestration was successful
    """
    data = request.json
    # data_dict = json.loads(data)
    first_name = data['first_name']
    last_name = data['last_name']
    role = data['role']

    insert_client_to_db(first_name, clients_table, role=role, last_name=last_name)

    data.pop('first_name')
    data.pop('last_name')
    data.pop('role')

    data['client_id'] = find_client_id(first_name, clients_table, last_name=last_name)
    acc = Account.deserialize(json_dict=data)

    resp = insert_account_to_db(acc, accounts_table)
    return Response(status=resp)


@app.route('/login', methods=['POST'])
def login():
    """
    Client's log in to system

    :return: response if log in was successful
    """
    data = request.json
    user_name = data['user_name']
    password = data['password']
    resp = check_password(user_name, password, accounts_table)
    return Response(status=resp)


@app.route('/client', methods=['POST'])
def insert_client():
    """
    Inserts new client to database by client's request

    :return: response if was successfully inserted
    """
    data = request.json
    json_dict = json.loads(data)
    name = json_dict['name']
    insert_client_to_db(name, clients_table)
    resp = jsonify(success=True)
    return resp


@app.route('/product', methods=['GET'])
def get_product_prices():
    """
    Gets all instrument with their prices by client's request

    :return: list of all instruments with their prices
    """
    resp = select_product_prices(instruments_table)
    return jsonify(resp)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=app.config['APP_PORT'], debug=True)
