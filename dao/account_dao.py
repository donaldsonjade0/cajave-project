from passlib.hash import sha256_crypt



def insert_account_to_db(account, accounts_table):
    """
    Inserting account information to database after registration a new account

    :param account: class object that should to be inserted
    :param accounts_table: table where to insert account
    :return: response if creating a new account was successful
    """
    acc_dict = account.__dict__
    unique = ['user_name']
    acc_dict['pass'] = acc_dict['passw']
    acc_dict.pop('passw')
    hashh = sha256_crypt.hash(acc_dict['pass'])
    acc_dict['pass'] = hashh
    resp = accounts_table.insert_if_not_exists(data_dict=acc_dict, unique_columns=unique)
    return resp


def check_password(user_name_, password_, accounts_table):
    """
    Checking if the password is right while sign in

    :param user_name_: client's user name
    :param password_:  client's input password
    :param accounts_table:
    :return: response if input password matches to real password
    """
    password_real = accounts_table.select(['pass'], {'user_name': user_name_})
    if len(password_real) > 0:
        if sha256_crypt.verify(password_, password_real[0][0]):
            return 200
        else:
            return 403
    else:
        return 403
