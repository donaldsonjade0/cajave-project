def insert_client_to_db(name, clients_table, role=None, last_name=None):
    """
    Inserting new client to database

    :param name: client's first name
    :param clients_table: table of clients
    :param role: client's role
    :param last_name: client's last name
    """
    unique = ['first_name', 'last_name']
    if last_name is None:
        last_name = ''
    data_dict = {'first_name': name, 'last_name': last_name, 'role': role}
    clients_table.insert_if_not_exists(data_dict, unique)


def find_client_id(first_name, clients_table, last_name=None):
    """
    Finding client id by first and last names

    :param first_name: client's first name
    :param clients_table: table of clients
    :param last_name: client's last name
    :return: client id
    """
    if last_name is None:
        last_name = ''
    resp = clients_table.select(['id'], {'first_name': first_name, 'last_name': last_name})
    return resp[0][0]
