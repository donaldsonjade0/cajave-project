def insert_instrument_to_db(instrument_name, instruments_table):
    """
    Inserts new instrument to database

    :param instrument_name: name of instrument
    :param instruments_table: instrument current price
    :return: None
    """
    instruments_table.insert({'name': instrument_name, 'current_price': 0.})

def select_product_prices(instruments_table):
    """
    Gets all instruments with their prices

    :param instruments_table: table of instruments
    :return: list of products with their prices
    """
    resp = instruments_table.select(['name', 'current_price'])
    return resp