import pytest
from deal import Deal


def test_deal_deserialize_success():
    json_str = '{"instrumentName": "Lunatic", "cpty": "John", "price": 1847.1347717720864, "type": "B", ' \
               '"quantity": 1, "time": "11-Aug-2019 (13:15:29.497437)"}'

    deal_expected = Deal(instrumentName='Lunatic', cpty='John', price=1847.1347717720864, type='B',
                         quantity=1, time="11-Aug-2019 (13:15:29.497437)", deal_id=None)

    deal_actual = Deal.deserialize(json_str)

    assert deal_actual.instrument_name == deal_expected.instrumentName
    assert deal_actual.cpty == deal_expected.cpty
    assert deal_actual.price == deal_expected.price
    assert deal_actual.type == deal_expected.type
    assert deal_actual.quantity == deal_expected.quantity
    assert deal_actual.deal_id == deal_expected.deal_id


def test_serialize():
    expected_str = '{"deal_id": null, "instrumentName": "Lunatic", "cpty": "John", "price": 1847.1347717720864, ' \
                   '"type": "B", "quantity": 1, "time": "11-Aug-2019 (13:15:29.497437)"}'

    deal = Deal(instrumentName='Lunatic', cpty='John', price=1847.1347717720864, type='B',
                quantity=1, time="11-Aug-2019 (13:15:29.497437)", deal_id=None)

    actual_str = deal.serialize()

    assert expected_str == actual_str


def test_deal_deserialize_error():
    try:
        json_str = '{"instrumentName": "Lunatic", "cpty": "John", "price": 1847.1347717720864, "type": "B", ' \
                   '"quantity": 1}'

        deal_actual = Deal.deserialize(json_str)  # will throw error

        raise Exception
    except Exception:
        assert True
