from deal import Deal
from client_dao import insert_client_to_db, find_client_id


def insert_to_inventory(client_id, inst_id, quantity, type, inventories_table):
    """
    Inserts information to inventory table

    :param client_id: client id
    :param inst_id: instrument id
    :param quantity: quantity
    :param type: type of deal
    :param inventories_table: table of inventories
    :return: None
    """
    resp = inventories_table.select(['count(*)'], {'client_id': client_id, 'instrument_id': inst_id})
    if type == 'B':
        if resp[0][0] == 0:
            inventories_table.insert({'client_id': client_id, 'instrument_id': inst_id, 'quantity': quantity})
        else:
            inventories_table.update('quantity', quantity, operation='+', where_data_dict={'client_id':client_id, 'instrument_id': inst_id})
    elif type == 'S':
        if resp[0][0] == 0:
            inventories_table.insert({'client_id': client_id, 'instrument_id': inst_id, 'quantity': quantity})
        else:
            inventories_table.update('quantity', quantity, operation='-', where_data_dict={'client_id':client_id, 'instrument_id': inst_id})


def insert_deal_to_db(deal, deals_table, instruments_table, clients_table, inventories_table):
    """
    Inserts new deals to database

    :param deal: Deal object to insert
    :param deals_table: table of Deals
    :param instruments_table: table of Instruments
    :param clients_table: table of Clients
    :param inventories_table: table of Inventories
    :return: None
    """

    instruments_table.insert_if_not_exists({'name': deal.instrumentName, 'current_price': 0}, ['name'])
    insert_client_to_db(deal.cpty, clients_table)

    inst_id = instruments_table.select(['id'], {'name': deal.instrumentName})[0][0]

    cpty_id = find_client_id(deal.cpty, clients_table)

    deals_table.insert(
        {'product_id': inst_id, 'cpty_id': cpty_id, 'price': deal.price, 'type': deal.type, 'quantity': deal.quantity,
         'creation_time': deal.time})

    instruments_table.update('current_price', deal.price, where_data_dict={'name': deal.instrumentName})

    insert_to_inventory(cpty_id, inst_id, deal.quantity, deal.type, inventories_table)


def select_deal_by_id(id, deals_table, instruments_table, clients_table):
    """
    Gets deal information by its id

    :param id: deal id
    :param deals_table: table of Deals
    :param instruments_table: table of Instruments
    :param clients_table: table of Clients
    :return: deal object that was found or None if there is no object with such id
    """
    resp = deals_table.select(['count(*)'], {'id': id})
    if resp[0][0] != 0:
        resp_list = deals_table.select(['product_id', 'cpty_id', 'quantity', 'price', 'type', 'creation_time'], {'id': id})

        product_id = resp_list[0][0]
        cpty_id = resp_list[0][1]

        inst_name = instruments_table.select(['name'], {'id': id})[0][0]

        cpty_name = clients_table.select(['first_name'], {'id': cpty_id})[0][0]
        deal = Deal(inst_name, cpty_name, resp_list[0][3], resp_list[0][4], resp_list[0][2],
                    resp_list[0][5].strftime("%d-%b-%Y (%H:%M:%S.%f)"), deal_id=id)
        return deal
    else:
        return None


def select_deals_by_name(name, deals_table, last_name=None):
    """
    Gets all deals that were made by client

    :param name: client's first_name
    :param deals_table: table of Deals
    :param last_name: client's last name
    :return: list of client's deals
    """
    if last_name is None:
        last_name = ''
    resp = deals_table.select([
                                  'Deals.id, Instruments.name, Clients.first_name, Deals.quantity, Deals.price, Deals.type, Deals.creation_time'],
                              {'Clients.first_name': name, 'Clients.last_name': last_name},
                              tables=['Deals', 'Instruments', 'Clients'],
                              where_statement='Clients.id = Deals.cpty_id and Instruments.id = Deals.product_id')
    deal_list = []
    for arr in resp:
        deal = Deal(arr[1], arr[2], arr[4], arr[5], arr[3], arr[6].strftime("%d-%b-%Y (%H:%M:%S.%f)"), deal_id=arr[0])
        deal_list.append(deal)
    return deal_list


def select_deals_by_product(product, deals_table):
    """
    Gets all deals with certain instrument
    :param product: instrument name
    :param deals_table: table of deal
    :return: list of deals
    """
    resp = deals_table.select([
                                  'Deals.id, Instruments.name, Clients.first_name, Deals.quantity, Deals.price, Deals.type, Deals.creation_time'],
                              {'Instruments.name': product}, tables=['Deals', 'Instruments', 'Clients'],
                              where_statement='Clients.id = Deals.cpty_id and Instruments.id = Deals.product_id')
    deal_list = []
    for arr in resp:
        deal = Deal(arr[1], arr[2], arr[4], arr[5], arr[3], arr[6].strftime("%d-%b-%Y (%H:%M:%S.%f)"), deal_id=arr[0])
        deal_list.append(deal)
    return deal_list
