import json
from datetime import datetime
from typing import List, Dict


class Deal:

    def __init__(self, instrumentName, cpty, price, type, quantity, time: datetime, deal_id=None):
        self.deal_id = deal_id
        self.instrumentName = instrumentName
        self.cpty = cpty
        self.price = price
        self.type = type
        self.quantity = quantity
        self.time = time

    @staticmethod
    def deserialize(json_str):
        json_dict = json.loads(json_str)
        time_str = json_dict['time']
        time_datetime = datetime.strptime(time_str, '%d-%b-%Y (%H:%M:%S.%f)')
        json_dict['time'] = time_datetime
        return Deal(deal_id=json_dict['deal_id'] if 'deal_id' is json_dict.keys() else None,
                    instrumentName=json_dict['instrumentName'],
                    cpty=json_dict['cpty'],
                    price=json_dict['price'],
                    type=json_dict['type'],
                    quantity=json_dict['quantity'],
                    time=json_dict['time'])

    def serialize(self):
        return json.dumps(self.__dict__)

    @staticmethod
    def _create_from_dict(json_dict: Dict) -> 'Deal':
        time_str = json_dict['time']
        time_datetime = datetime.strptime(time_str, '%d-%b-%Y (%H:%M:%S.%f)')
        json_dict['time'] = time_datetime
        return Deal(deal_id=json_dict['deal_id'] if 'deal_id' is json_dict.keys() else None,
                    instrumentName=json_dict['instrumentName'],
                    cpty=json_dict['cpty'],
                    price=json_dict['price'],
                    type=json_dict['type'],
                    quantity=json_dict['quantity'],
                    time=json_dict['time'])

    @staticmethod
    def parse_json_list(deals_json) -> List['Deal']:
        deals_dict: List[Dict] = json.loads(deals_json)
        return list((Deal._create_from_dict(json.loads(x)) for x in deals_dict))
