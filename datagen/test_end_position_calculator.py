from datetime import datetime, timedelta

from deal import Deal
from end_positions_calculator import EndPositionCalculator

deals = [Deal('apple', 'John', 100, 'B', 10, datetime.now()),
         Deal('apple', 'John', 110, 'S', 5, datetime.now() + timedelta(minutes=1)),
         Deal('orange', 'John', 120, 'B', 10, datetime.now() + timedelta(minutes=2)),
         Deal('book', 'John', 120, 'S', 10, datetime.now() + timedelta(minutes=6)),
         Deal('book', 'John', 130, 'B', 6, datetime.now() + timedelta(minutes=6))]

current_prices = {'apple': 120, 'orange': 100, 'book': 131}


def test_calculate_end_position():
    deals = [Deal('apple', 'John', 100, 'B', 10, datetime.now()),
             Deal('apple', 'John', 110, 'S', 5, datetime.now() + timedelta(minutes=1)),
             Deal('orange', 'John', 120, 'B', 10, datetime.now() + timedelta(minutes=2)),
             Deal('book', 'John', 120, 'S', 10, datetime.now() + timedelta(minutes=6)),
             Deal('book', 'John', 130, 'B', 6, datetime.now() + timedelta(minutes=6))]

    current_prices = {'apple': 120, 'orange': 100, 'book': 131}

    expected_result = {'apple': (10, 5), 'orange': (10, 0), 'book': (6, 10)}

    actual_result = EndPositionCalculator.calculate_end_position(deals, current_prices)
    print(actual_result)

    assert actual_result.__eq__(expected_result)