import numpy


class Instrument:
    def __init__(self, id, name, __starting_price, __price, __variance, __drift):
        self.id = id
        self.name = name
        self.__startingPrice = __starting_price
        self.__price = __price
        self.__variance = __variance
        self.__drift = __drift

    def calculate_next_price(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0, 1) * self.__variance + self.__drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < self.__startingPrice * 0.4:
            self.__drift = (-0.7 * self.__drift)
        self.__price = newPrice * 1.01 if direction == 'B' else newPrice * 0.99
        return self.__price

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def get_price(self):
        return self.__price

    def get_variance(self):
        return self.__variance

    def get_drift(self):
        return self.__drift
