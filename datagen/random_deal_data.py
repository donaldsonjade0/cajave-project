import time
import random
from datetime import datetime, timedelta
import json
from instrument import *

instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
               "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")
NUMBER_OF_RANDOM_DEALS = 2000
TIME_PERIOD_MILLIS = 3600000
EPOCH = datetime.now() - timedelta(days=1)


class RandomDealData:

    @staticmethod
    def create_instrument_list():
        f = open('initialRandomValues.txt', 'r')
        instrumentId = 1000
        instrumentList = []
        for instrumentName in instruments:
            hashed_value = int(f.readline())
            is_negative = hashed_value < 0
            basePrice = (abs(hashed_value) % 10000) + 90.0
            drift = ((abs(hashed_value) % 5) * basePrice) / 1000.0
            drift = 0 - drift if is_negative else drift
            variance = (abs(hashed_value) % 1000) / 100.0
            variance = 0 - variance if is_negative else variance
            instrument = Instrument(instrumentId, instrumentName, basePrice, basePrice, variance, drift)
            instrumentList.append(instrument)
            instrumentId += 1
        return instrumentList

    @staticmethod
    def create_random_data(instrument_list):
        time.sleep(random.uniform(1, 30) / 100)
        dealId = 20000
        instrument = instrument_list[numpy.random.randint(0, len(instrument_list))]
        cpty = counterparties[numpy.random.randint(0, len(counterparties))]
        type = 'B' if numpy.random.choice([True, False]) else 'S'
        quantity = int(numpy.power(1001, numpy.random.random()))
        dealTime = datetime.now() - timedelta(days=1)
        dealId += 1
        deal = {
            'instrumentName': instrument.name,
            'cpty': cpty,
            'price': instrument.calculate_next_price(type),
            'type': type,
            'quantity': quantity,
            'time': dealTime.strftime("%d-%b-%Y (%H:%M:%S.%f)"),
        }
        return json.dumps(deal)
