import json
from datetime import datetime
from typing import List, Dict


class Deal:
    """
    Class that contains information about deal
    """

    def __init__(self, instrumentName, cpty, price, type, quantity, time: datetime, deal_id=None):
        """
        Initializing deal

        :param instrumentName: name of instrument
        :param cpty: counter party
        :param price: deal's price
        :param type: type of deal, buy or sell
        :param quantity: quantity of instruments
        :param time: time at what deal was done
        :param deal_id: deal id, could be None than it will be autoincrement in database
        """
        self.deal_id = deal_id
        self.instrumentName = instrumentName
        self.cpty = cpty
        self.price = price
        self.type = type
        self.quantity = quantity
        self.time = time

    @staticmethod
    def deserialize(json_str):
        """
        Making Deal object from json string

        :param json_str: json string with deal information
        :return: deal object with deal infomation from json
        """
        print(json_str)
        json_dict = json.loads(json_str)
        time_str = json_dict['time']
        time_datetime = datetime.strptime(time_str, '%d-%b-%Y (%H:%M:%S.%f)')
        json_dict['time'] = time_datetime
        return Deal(deal_id=json_dict['deal_id'] if 'deal_id' is json_dict.keys() else None,
                    instrumentName=json_dict['instrumentName'],
                    cpty=json_dict['cpty'],
                    price=json_dict['price'],
                    type=json_dict['type'],
                    quantity=json_dict['quantity'],
                    time=json_dict['time'])

    def serialize(self):
        """
        Making json file from deal

        :return: json file with deal information
        """
        return json.dumps(self.__dict__)

    @staticmethod
    def _create_from_dict(json_dict: Dict) -> 'Deal':
        """
        Creating deal from dictionary

        :param json_dict: dictionary with deal information
        :return: Deal object with information from dictionary
        """
        time_str = json_dict['time']
        time_datetime = datetime.strptime(time_str, '%d-%b-%Y (%H:%M:%S.%f)')
        json_dict['time'] = time_datetime
        return Deal(deal_id=json_dict['deal_id'] if 'deal_id' is json_dict.keys() else None,
                    instrumentName=json_dict['instrumentName'],
                    cpty=json_dict['cpty'],
                    price=json_dict['price'],
                    type=json_dict['type'],
                    quantity=json_dict['quantity'],
                    time=json_dict['time'])

    @staticmethod
    def parse_json_list(deals_json) -> List['Deal']:
        """
        Making list of Deal objects form json

        :param deals_json: json file with list of deal information
        :return: list of deals
        """
        deals_dict: List[Dict] = json.loads(deals_json)
        return list((Deal._create_from_dict(json.loads(x)) for x in deals_dict))

    def is_buy_deal(self):
        return self.type == 'B'
