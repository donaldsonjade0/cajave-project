from deal import Deal
from random_deal_data import RandomDealData
from threading import Event, Thread


class DatagenService:
    """
    Class for data generation
    """
    def __init__(self):
        """
        Initializing
        """
        self._gen_thread = Thread(target=self._generate_loop)
        self.deal_data = RandomDealData()
        self.instruments = self.deal_data.create_instrument_list()  # TODO (avlomakin) : GET instruments from Database
        self._event = Event()
        self._new_deal_handlers = []
        self._lastDeal = None
        self._stop_event = Event()

    def _generate_loop(self):
        """
        Constantly generates new deals

        :return: None
        """
        while True:
            self._event.clear()

            if self._stop_event.is_set():
                return

            deal = self.deal_data.create_random_data(self.instruments)
            for handler in self._new_deal_handlers:
                handler(deal)

            self._lastDeal = deal
            self._event.set()

    def add_handler(self, handler):
        """
        Add deals handler

        :param handler: handler
        :return: None
        """
        self._new_deal_handlers.append(handler)

    def get_deal(self):
        """
        Gets last deal

        :return: last deal
        """
        self._event.wait()
        return self._lastDeal

    def start_generating_deals(self):
        """
        Starts deals generation

        :return: None
        """
        self._stop_event.clear()
        self._gen_thread.start()

    def stop_generating_deals(self):
        """
        Ends deals generation

        :return: None
        """
        self._stop_event.set()
