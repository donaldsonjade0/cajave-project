import pytest
from database_client import DatabaseClient


def test_save_deal_to_database():
    def dummy_post(endpoint, json):
        assert json == expected_deal

    expected_deal = "dummy"
    client = DatabaseClient("endpoint", post_method=dummy_post)
    client.save_deal_to_db(expected_deal)