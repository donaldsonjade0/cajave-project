from datetime import timedelta
from flask import Flask, Response, jsonify

from avg_price_calculator import AvgPriceCalculator
from database_client import DatabaseClient
from datagen_service import DatagenService
from end_positions_calculator import EndPositionCalculator
from profit_calculator import ProfitCalculator
import os

app = Flask(__name__)


@app.route('/', methods=['GET'])
def main():
    """
    API method to get stream with all live deals

    :return: stream with live deals
    """

    def deal_stream():
        while True:
            yield 'data: ' + datagenSvc.get_deal() + '\n\n'

    return Response(deal_stream(), mimetype="text/event-stream")


@app.route('/end_position/<trader_name>', methods=['GET'])
def get_end_position(trader_name):
    deals = dao_client.get_deals_by_trader_name(trader_name)
    positions = EndPositionCalculator.calculate_end_position(deals, {})
    return positions


@app.route('/calc_avg_price/<product_name>', methods=['GET'])
def calc_avg_price(product_name):
    """
    Calculates average price of instrument

    :param product_name: instrument name
    :return: average price
    """

    deals = dao_client.get_deals_by_product_name(product_name)
    calculator = AvgPriceCalculator(time_frame=timedelta(minutes=1))
    result = calculator.calc_avg_price(deals)

    return jsonify(result)


@app.route('/profit/<trader_name>', methods=['GET'])
def get_profits(trader_name):
    """
    Gets client's profit

    :param trader_name: client's name
    :return: client's profit
    """

    deals = dao_client.get_deals_by_trader_name(trader_name)
    realised = ProfitCalculator.calc_realised_profit(deals)

    product_prices = dao_client.get_product_prices()
    effective = ProfitCalculator.calc_effective_profit(deals, product_prices)

    result = {'realized': realised, 'effective': effective}
    return result


if __name__ == '__main__':

    if os.name == 'nt':
        app.config['DAO_URL'] = 'http://127.0.0.1:5002'
        app.config['SAVE_DEAL_TO_DB'] = False
        app.config['APP_PORT'] = 5003
    else:
        app.config['DAO_URL'] = os.environ['DAO_URL']
        app.config['SAVE_DEAL_TO_DB'] = os.environ['SAVE_DEAL_TO_DB']
        app.config['APP_PORT'] = os.environ['APP_PORT']

    datagenSvc = DatagenService()

    dao_client = DatabaseClient(app.config['DAO_URL'])

    #if app.config['SAVE_DEAL_TO_DB']:
    #    datagenSvc.add_handler(dao_client.save_deal_to_db)

    datagenSvc.start_generating_deals()
    app.run(host='0.0.0.0', port=app.config['APP_PORT'])
