import json
from typing import List, Dict

import requests
from deal import Deal


class DatabaseClient:
    """
    Class that helps get information from DAO
    """
    def __init__(self, dao_endpoint: str, get_method=requests.get, post_method=requests.post):
        """
        Initializing

        :param dao_endpoint: IP address of DAO
        :param get_method: methods for get requests
        :param post_method: methods for post_requests
        """
        self._post_method = post_method
        self._get_method = get_method
        self._dao_endpoint = dao_endpoint

    @property
    def _deals_endpoint(self):
        """
        Gets IP address for deal requests

        :return: IP address
        """
        return self._dao_endpoint + '/deal'

    @property
    def _product_endpoint(self):
        """
        Gets IP address for product requests

        :return: IP address
        """
        return self._dao_endpoint + '/product'

    def save_deal_to_db(self, deal: str):
        """
        Saves deal to database

        :param deal: deal
        :return: None
        """
        self._post_method(self._deals_endpoint, json=deal)

    def get_product_by_name(self, name):
        """
        Get instrument information by its name

        :param name: instrument name
        :return: instrument information
        """
        return self._get_method(self._product_endpoint + f'/{name}')

    def get_deals_by_product_name(self, product_name) -> List[Deal]:
        """
        Gets all deals with certain instrument

        :param product_name: instrument name
        :return: list of deals
        """
        deals_json = self._get_method(self._deals_endpoint + f'/product/{product_name}')
        deals = Deal.parse_json_list(deals_json.content)
        return deals

    def get_deals_by_trader_name(self, trader_name) -> List[Deal]:
        """
        Gets all client's deals

        :param trader_name: client's name
        :return: list of deals
        """
        deals_json = self._get_method(self._deals_endpoint + f'/name/{trader_name}')
        deals = Deal.parse_json_list(deals_json.content)
        return deals

    def get_product_prices(self) -> Dict[str, float]:
        """
        Gets all instruments with their prices

        :return: instruments with their prices
        """
        products_json = self._get_method(self._product_endpoint)
        product_list = (json.loads(products_json.content))
        result = dict((x[0], x[1]) for x in product_list)
        return result
