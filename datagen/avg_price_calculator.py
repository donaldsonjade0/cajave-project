from datetime import timedelta
from typing import List, Tuple

from deal import Deal


class AvgPriceCalculator:
    """
    Calculator of average price
    """

    def __init__(self, time_frame: timedelta =timedelta(minutes=5)):
        """
        Initializing calculator

        :param time_frame: tame frame for which average price is calculated
        """
        self._time_frame = time_frame

    def calc_avg_price(self, deals: List[Deal]) -> List[Tuple[int, float]]:
        """
        Calculating average price

        :param deals: list of deals
        :return: average price of deals
        """
        deals.sort(key=lambda x: x.time)
        result = []

        start_time = deals[0].time
        current_chunk = []
        i = 0

        for deal in deals:
            if start_time + self._time_frame > deal.time:
                current_chunk.append(deal)
            else:
                chunk_avg_price = sum(d.price for d in current_chunk) / len(current_chunk)
                result.append((i, chunk_avg_price))
                current_chunk = [deal]
                start_time += self._time_frame
                i += 1

        chunk_avg_price = sum(d.price for d in current_chunk) / len(current_chunk)
        result.append((i, chunk_avg_price))

        return result
