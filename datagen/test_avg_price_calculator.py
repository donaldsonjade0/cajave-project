from datetime import datetime, timedelta

from avg_price_calculator import AvgPriceCalculator
from deal import Deal

deals = [Deal('test', '2', 100, 'B', 2, datetime.now()),
         Deal('test', '2', 110, 'B', 2, datetime.now() + timedelta(minutes=1)),
         Deal('test', '2', 120, 'B', 2, datetime.now() + timedelta(minutes=2)),
         Deal('test', '2', 130, 'B', 2, datetime.now() + timedelta(minutes=6)),
         Deal('test', '2', 140, 'B', 2, datetime.now() + timedelta(minutes=7)),
         Deal('test', '2', 150, 'B', 2, datetime.now() + timedelta(minutes=8)),
         Deal('test', '2', 160, 'B', 2, datetime.now() + timedelta(minutes=11)),
         Deal('test', '2', 170, 'B', 2, datetime.now() + timedelta(minutes=12)),
         Deal('test', '2', 180, 'B', 2, datetime.now() + timedelta(minutes=13))]

def test_calc_avg_price():
    calc = AvgPriceCalculator()

    expected_result = [(0, 110), (1, 140), (2, 170)]
    actual_result = calc.calc_avg_price(deals)

    print(expected_result)
    print(actual_result)

    assert expected_result.__eq__(actual_result)
