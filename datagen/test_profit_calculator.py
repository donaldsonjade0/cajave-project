from datetime import datetime, timedelta

from deal import Deal
from profit_calculator import ProfitCalculator


def test_calc_realised_profit():
    deals = [Deal('apple', 'John', 100, 'B', 1, datetime.now()),
             Deal('apple', 'John', 110, 'S', 1, datetime.now() + timedelta(minutes=1)),
             Deal('orange', 'John', 120, 'B', 1, datetime.now() + timedelta(minutes=2)),
             Deal('book', 'John', 120, 'S', 1, datetime.now() + timedelta(minutes=6)),
             Deal('book', 'John', 130, 'B', 1, datetime.now() + timedelta(minutes=6))]
    expected_profit = -120
    actual_profit = ProfitCalculator.calc_realised_profit(deals)
    assert expected_profit == actual_profit


def test_calc_effective_profit():
    deals = [Deal('apple', 'John', 100, 'B', 1, datetime.now()),
             Deal('apple', 'John', 110, 'S', 1, datetime.now() + timedelta(minutes=1)),
             Deal('orange', 'John', 120, 'B', 1, datetime.now() + timedelta(minutes=2)),
             Deal('book', 'John', 120, 'S', 1, datetime.now() + timedelta(minutes=6)),
             Deal('book', 'John', 130, 'B', 1, datetime.now() + timedelta(minutes=6))]
    current_product_prices = {'orange': 150}
    expected_profit = 30
    actual_profit = ProfitCalculator.calc_effective_profit(deals, current_product_prices)
    assert expected_profit == actual_profit
