from datagen_service import DatagenService
import threading


def test_add_handler_ok():
    e = threading.Event()

    def dummy_handler(deal):
        nonlocal e
        assert deal is not None
        e.set()

    svc = DatagenService()
    svc.add_handler(dummy_handler)
    svc.start_generating_deals()
    e.wait()
    svc.stop_generating_deals()


def test_add_handler_data_from_get_deal_eq_data_from_handler():
    deal_from_handler = ""

    def dummy_handler(deal):
        nonlocal deal_from_handler
        deal_from_handler = deal
        assert deal is not None

    svc = DatagenService()
    svc.add_handler(dummy_handler)
    svc.start_generating_deals()
    deal_from_get_deal = svc.get_deal()
    assert deal_from_handler == deal_from_get_deal
    svc.stop_generating_deals()
