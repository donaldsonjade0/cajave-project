from typing import List, Dict

from deal import Deal


class ProfitCalculator:
    """
    Profit Calculator for each client
    """
    @staticmethod
    def _calc_sales_profit(deals: List[Deal]):
        """
        Calculates sales profit

        :param deals: list of deals
        :return: sales profit
        """
        return sum((x.price * x.quantity) if x.type == 'S' else 0 for x in deals)

    @staticmethod
    def _calc_purchase_spending(deals: List[Deal]):
        """
        Calculates purchase spending

        :param deals: list of deals
        :return: purchase spending
        """
        return sum((x.price * x.quantity) if x.type == 'B' else 0 for x in deals)

    @staticmethod
    def calc_realised_profit(deals: List[Deal]):
        """
        Calculates realised profit

        :param deals: list of deals
        :return: realised profit
        """
        return ProfitCalculator._calc_sales_profit(deals) - ProfitCalculator._calc_purchase_spending(deals)

    @staticmethod
    def calc_effective_profit(deals: List[Deal], current_product_prices: Dict[str, float]):
        """
        Calculates effective profit

        :param deals: list of deals
        :param current_product_prices: current product price
        :return: effective profit
        """
        realised_profit = ProfitCalculator.calc_realised_profit(deals)
        inventory = ProfitCalculator._get_current_inventory(deals)
        stock_value = 0
        for instrument in inventory.keys():
            if inventory[instrument] is not 0:
                stock_value += inventory[instrument] * current_product_prices[instrument]

        return realised_profit + stock_value


    @staticmethod
    def _get_current_inventory(deals: List[Deal]) -> Dict[str, int]:
        """
        Gets current inventory
        :param deals: list of deals
        :return: list of how much instruments of each type are in inventory
        """
        inventory: Dict[str, int] = {}
        for deal in deals:
            if deal.instrumentName in inventory.keys():
                inventory[deal.instrumentName] += deal.quantity if deal.type == 'B' else -deal.quantity
            else:
                inventory[deal.instrumentName] = deal.quantity if deal.type == 'B' else -deal.quantity

        return inventory
