from typing import List, Dict, Tuple

from deal import Deal


class EndPositionCalculator:

    def __init__(self):
        pass

    @staticmethod
    def calculate_end_position(deals : List[Deal], current_prices: Dict[str, int]) -> Dict[str, Tuple[int, int]]:
        result = {}
        for deal in deals:
            if deal.instrumentName not in result.keys():
                result[deal.instrumentName] = (0, 0)
            current_deal_tuple = (deal.quantity, 0) if (deal.is_buy_deal()) else (0, deal.quantity)

            result[deal.instrumentName] = tuple(sum(x) for x in zip(result[deal.instrumentName], current_deal_tuple))

        return result
