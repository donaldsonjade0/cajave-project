import React, {Component} from 'react';
import { Line } from 'react-chartjs-2';
import '../styling/historicalData.css';

const data = {
  labels: [],
  datasets: [
    {
      label: '',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],   
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    }
  ]
};

const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        yAxes: [{
            scaleLabel : {
                display: true,
                labelString: 'Price'
            }
        }],
        xAxes: [{
            scaleLabel : {
                display: true,
                labelString: 'Days'
            }
        }]
    }
}

export default class HistoricalData extends Component {

    constructor (props){
        super(props);
      
        this.state = {
            loopActive: false,
            shuffleActive: false,
          };
      
        this.generateGraph = this.generateGraph.bind(this)
      
      }

    generateGraph(e){
        let instrumentName = e.target.value;
        console.log('http://ws-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com/api/avg_price/'+instrumentName)
        fetch('http://ws-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com/api/avg_price/'+instrumentName).then(function(response){
            data.datasets[0].data = []
            data.labels = []
            response.json().then(function(jss){
                for(let arr in jss){
                    data.datasets[0].data.push(jss[arr][1])
                    data.labels.push(jss[arr][0])
                    console.log(data)
                }
            }
            )       
            return response.body;  
        })
        
        this.refs.chart.setState({ state: this.refs.chart.state })
        // console.log(datasets[0].data);
      }

    render() {
        return (
        <>
            <h2>Product avg price for Instrument</h2>
            <Line ref="chart" data={data} options={options} id="lineChart" /> 
            <div>
                <form>
                    <select value={this.value} onChange={this.generateGraph.bind(this.value)} name="Instrument">
                    <option value="Floral">Floral</option>
                    <option value="Eclipse">Eclipse</option>
                    <option value="Astronomica">Astronomica</option>
                    <option value="Borealis">Borealis</option>
                    <option value="Celestial">Celestial</option>
                    <option value="Jupiter">Jupiter</option>
                    <option value="Koronis">Koronis</option>
                    <option value="Interstella">Interstella</option>
                    <option value="Lunatic">Lunatic</option>
                    <option value="Heliosphere">Heliosphere</option>
                    <option value="Galactia">Galactia</option>
                    <option value="Deuteronic">Deuteronic</option>

                    </select>
                </form>
            </div>
            
        </>

        );
    }

    // componentDidMount(){
    //     fetch('http://localhost:5001/api/avg_price/Eclipse').then(function(response){
    //         data.datasets[0].data = []
    //         data.labels = []
    //         response.json().then(function(jss){
    //             for(let arr in jss){
    //                 data.datasets[0].data.push(jss[arr][1])
    //                 data.labels.push(jss[arr][0])
    //                 console.log(data)
    //             }
    //         }
    //         )       
    //         return response.body;    
    //     })
    // }

}