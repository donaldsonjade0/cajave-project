import React from 'react';
import '../styling/login.css';
import Deals from '../pages/DataStream';

export default class Login extends React.Component {

    constructor(props){
        super(props);

        // Set initial value of fields.
        this.state = {
            email:"",
            password:"",
            loginStatus: false
        };

    }

    // get username from form.
    handleUsernameChange(e){
        this.setState({username: e.target.value});
    }

    // get password from form.
    handlePasswordChange(e){
        this.setState({password: e.target.value});
    }

    // Call API with login details (TO DO)
    handleSubmit(event){
        event.preventDefault(); // Stop parameters being passed through url.
        console.log(this.state.username)
        console.log(this.state.password)

        var data = {"username": this.state.username, "password": this.state.password};
        

        fetch("http://ws-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com/api/login", {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(res => {
                
                if (res.status == 200) {
                    alert("Login successful, connected to database.");
                    this.setState({loginStatus: true});
                } else {
                    alert("Wrong login, please try again.")
                }
            })
    }

    renderLogin(){
        return (
            <div id="login_form">
                <form onSubmit={this.handleSubmit} class="text-center border border-light p-5">
                    <p class="h4 mb-4">Sign in</p>
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label"> Username: </label>
                        <input onChange={this.handleUsernameChange.bind(this)} type="text" class="form-control mb-4" name="username"></input>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label"> Password:  </label>
                        <input onChange={this.handlePasswordChange.bind(this)} type="password" class="form-control mb-4" name="password"></input>
                    </div>
                    <button class="btn btn-info btn-block my-4" onClick={this.handleSubmit.bind(this)}>Submit</button>
                </form>
            </div>
        )
    }

    redirect(){
        return(
            <div>
               <Deals />
            </div>
        )
    }
    
    render() {
        if(this.state.loginStatus){
            return this.redirect();
        } else {
            return this.renderLogin();
        }
    }
}
