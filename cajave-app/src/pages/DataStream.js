import React, { useState } from 'react';
import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { map, withLatestFrom, delay } from 'rxjs/operators';
import SplitPane from 'react-split-pane';

import Charts from '../components/Charts';
import TableRow from '../components/TableRow';
import {MDBDataTable, MDBTableBody, MDBTableHead, MDBTable} from 'mdbreact';

import '../styling/datastream.css';
import { checkServerIdentity } from 'tls';
import TableFilter from 'tablefilter';
import { Table, Input } from 'reactstrap';
import ReactTable from 'react-table';

export default function DataStream(){
    
    const stringObservable = Observable.create(observer => {
        const source = new EventSource("http://ws-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com/api/deals");
        source.addEventListener('message', (messageEvent) => {
        //   console.log(messageEvent);
          observer.next(messageEvent.data);
        }, false);
      });


      const [stringArray, setStringArray] = useState([]);

        useObservable(
            state =>
            stringObservable.pipe(
                withLatestFrom(state),
                map(([state]) => {
                    console.log(state);
                let updatedStringArray = stringArray;
                
                updatedStringArray.unshift(JSON.parse(state));
                if (updatedStringArray.length >= 50) {
                    updatedStringArray.pop();
                } 
                setStringArray(updatedStringArray);
                //console.log(stringArray);
                return state;
                })
            )
        );
                    
        return (
            /* 
             <div>
                 <MDBTable>
                     <MDBTableHead>
                         <tr>
                             <th>Instrument</th>
                             <th>Counterparty</th>
                             <th>Price</th>
                             <th>Quantity</th>
                             <th>Type</th>
                             <th>Time</th>
                        </tr>
                     </MDBTableHead>
                     <MDBTableBody>
                        {stringArray.map((message, index) => <TableRow key={index} message={message}/>)}
                     </MDBTableBody>
                 </MDBTable>
             </div> */
            
             <div>
                 <h1>Deals Live-Stream</h1>
             <Table striped bordered hover variant='dark'>
                 <thead>
                     <tr>
                         <th>Instrument</th>
                         <th>Counterparty</th>
                         <th>Price</th>
                         <th>Quantity</th>
                         <th>Type</th>
                        <th>Time</th>
                     </tr>
                 </thead>
                 <tbody>
                     {stringArray.map((message, index) => <TableRow key={index} message={message}/>)}
                 </tbody>
            </Table>
            </div>
        );

}

