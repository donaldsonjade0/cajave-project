import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../styling/register.css'
import Login from '../pages/Login.js'

export default class Register extends React.Component{
    constructor(props){
        super(props);

        // Set initial value of fields.
        this.state = {
            email:"",
            password:"",
            registrationStatus: false
        };
    }

    inputFirstName(e){
        this.setState({firstname: e.target.value});
    }

    inputLastName(e){
        this.setState({lastname: e.target.value});
    }

    inputEmail(e){
        this.setState({email: e.target.value});
    }

    inputRole(e){
        this.setState({role: e.target.value});
    }

    inputUsername(e){
        this.setState({username: e.target.value});
    }

    inputPassword(e){
        this.setState({password: e.target.value});
    }

    handleSubmit(e){
        e.preventDefault();
        var data = {
            "firstname": this.state.firstname,
            "lastname": this.state.lastname,
            "email": this.state.email,
            "role": this.state.role,
            "username": this.state.username,
            "password": this.state.password
        };
        fetch("http://ws-team11-case-study.apps.dbgrads-6eec.openshiftworkshop.com/api/register", {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(res => {
                
                if (res.status == 200) {
                    alert("Registration successful, please login.");
                    this.setState({registrationStatus: true});

                } else {
                    alert("Bad registration, please try again.")
                }
            })
    }

    renderRegistration(){
        return(
        <div id="register_form">
        <form>
            <p class="h4 mb-4">Register</p>
                <div class="form-group">
                    <label>First Name: </label>
                    <input class="form-control" onChange={this.inputFirstName.bind(this)} type="text" name="firstName"></input>
                </div>
                <div class="form-group">
                    <label>Last Name:</label>
                    <input class="form-control" onChange={this.inputLastName.bind(this)} type="text" name="lastName" ></input>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" onChange={this.inputEmail.bind(this)} type="email" name="email" placeholder="name@example.com"></input>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <input class="form-control" onChange={this.inputRole.bind(this)} type="text" name="role"></input>
                </div>
                <div class="form-group">
                    <label>Username: </label>
                    <input class="form-control" onChange={this.inputUsername.bind(this)} type="text" name="username"></input>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" onChange={this.inputPassword.bind(this)} type="password" name="userpasswordname"></input>
                </div>
                
                <button class="btn btn-info btn-block my-4" onClick={this.handleSubmit.bind(this)}>Submit</button>
            </form>
        </div>
        )}

        redirectToLogin() {
            return(
                <div>
                    <Login />
                </div>
            )
        }

        render() {
            if(this.state.registrationStatus) {
                return this.redirectToLogin();
            } else {
                return this.renderRegistration();
            }
        }
}