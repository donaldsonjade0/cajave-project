import React from 'react';
import {Switch, Route, Link, BrowserRouter} from 'react-router-dom';
import Login from '../pages/Login'
import Register from '../pages/Register';
import Deals from '../pages/DataStream';
import HistoricalData from '../pages/HistoricalData';
import 'bootstrap/dist/css/bootstrap.css';
import '../styling/navigation.css'
import Logo from '../images/deutsche-bank-logo.jpg';

export default class Navigation extends React.Component {
    render(){
        return(
            <BrowserRouter>
                <div>
                <nav class="navbar navbar-expand-lg navbar-light" id="navbar">
                <span class="navbar-brand" href="#">
                    <img src={Logo} width="30" height="30" className="d-inline-block align-top" alt="db logo"></img>
                </span>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={'/'} className="nav-link">Login<span className="sr-only"></span></Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/Register'} className="nav-link">Register<span className="sr-only"></span></Link>
                        </li>
                        <li>
                            <Link to={'/Deals'} className="nav-link">Deals<span className="sr-only"></span></Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/historic'} className="nav-link">Historical Data<span className="sr-only"></span></Link>
                        </li>
                    </ul>
                </div>
                    </nav>
                    <hr />
                    <Switch>
                        <Route exact path='/' component={Login} />
                        <Route path='/register' component={Register} />
                        <Route path='/deals' component={Deals}/>
                        <Route path="/historic" component={HistoricalData} />
                    </Switch>
                </div>
            </BrowserRouter>
            );  
    }
}