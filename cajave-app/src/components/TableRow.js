import React from 'react';

const TableRow = props => (
    <tr>
        <td>{props.message.instrumentName}</td>
        <td>{props.message.cpty}</td>
        <td>{props.message.price}</td>
        <td>{props.message.quantity}</td>
        <td>{props.message.type}</td>
        <td>{props.message.time}</td>
    </tr>
);

export default TableRow;
