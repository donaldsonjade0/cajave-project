import React from 'react';
import {Line} from 'react-chartjs-2';

const Chart = props => {

    const data = {
        labels: [(props.message.timestamp)],
        datasets :[
            {
                label: 'Price',
                data: [(props.message.price)],
                fill: false,
                borderColor: 'green'
            }
        ]
    }
    return(
        <div className="chart">
            <Line data={data} />
        </div>   
    )
}

export default Chart;