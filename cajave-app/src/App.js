import React from "react";
import {BrowserRouter} from "react-router-dom";
import Navigation from './components/Navigation';

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Navigation />
            </BrowserRouter>
        )
    }
}

export default App;
