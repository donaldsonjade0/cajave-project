--Database Creation--

CREATE DATABASE Case_Study_db;
USE Case_Study_db;

--Table Creation--

create table Clients(
    id int auto_increment not null,
    first_name varchar(100) not null,
    last_name varchar(100) not null default '',
    role varchar(100),

    primary key (id)
);

create table Accounts(
	id int auto_increment not null,
	user_name varchar(100) not null unique,
    pass varchar(100) not null,
    email varchar(100) not null,
    client_id int not null,

    primary key (id),

    foreign key (client_id)
    references Clients(id)
);



create table Instruments(
    id int auto_increment not null,
    name varchar(100) unique,
    current_price float not null,

    primary key (id)
);

create table Deals(
    id int not null auto_increment,
    product_id int not null,
    cpty_id int not null,
    quantity int not null,
    price float not null,
    type varchar(1) not null,
    creation_time datetime,

    primary key (id),

    foreign key (cpty_id)
    references Clients(id),

    foreign key (product_id)
    references Instruments(id)
);


create table Inventories(
    id int not null auto_increment,
    client_id int not null,
    instrument_id int,
    quantity int,

    primary key (id),

    foreign key (client_id)
    references Clients(id)
);

